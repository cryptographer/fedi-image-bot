# Fedi Image Bot
This is a bot written in javascript that posts an image to the fediverse each time it's run.

## Usage
Simply place a directory named `images` into the directory this script is located in. Then edit the `instance_url` and `access_token` to match your settings. It will only post each image once, and keeps track of these filenames in `posted.txt`.
