const fs = require('fs');
const path = require('path');
const axios = require('axios');
const FormData = require('form-data');

// settings
const instance_url = 'INSTANCE_URL';
const access_token = 'ACCESS_TOKEN';

const post_visibility = 'public'; // public, unlisted, private or direct
const allowed_extensions = ['jpg', 'png', 'gif'];

const headers = {
    Authorization: 'Bearer ' + access_token
};

function selectImage() {
    // get image file names
    const dir_entries = fs.readdirSync(path.resolve(__dirname, 'images'), {withFileTypes: true});
    const filenames = dir_entries.filter(entry => entry.isFile()).map(entry => entry.name);
    const filtered_filenames = filenames.filter(file => allowed_extensions.includes(file.split('.').pop().toLowerCase()))

    // open or create posted file
    let fd = fs.openSync(path.resolve(__dirname, 'posted.txt'), 'as+');
    let posted = fs.readFileSync(fd);

    // confirm image file hasn't been posted yet and return path
    for(let file of filtered_filenames) {
        if(!posted.includes(file)) {
            fs.appendFileSync(fd, file + '\n');
            console.log('Posting:', file);
            return path.resolve(__dirname, 'images', file);
        }
    }

    console.log('No new images available.');
}

async function postImage(image_path) {
    const img = fs.readFileSync(image_path);

    const form = new FormData();

    const ext = image_path.split('.').pop();
    form.append('file', img, 'image.' + ext);

    const response = await axios.post(
        instance_url + '/media',
        form,
        {
            headers: {
                ...form.getHeaders(),
                ...headers
            }
        }
    );

    if(response.data.type !== 'unknown') {
        const res = await axios.post(
            instance_url + '/statuses',
            {
                media_ids: [response.data.id],
                visibility: post_visibility
            },
            {
                headers: headers
            }
        );
    }
}

const image_path = selectImage();

if(image_path) {
    postImage(image_path);
}
